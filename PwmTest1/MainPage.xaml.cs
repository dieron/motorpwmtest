﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.IoT.Devices.Pwm;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace PwmTest1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
	    private SoftPwm _pwm;
	    private GpioController _gpio;
	    private GpioPin _pinIn1;
	    private GpioPin _pinIn2;

	    public MainPage()
        {
            this.InitializeComponent();

			_gpio = GpioController.GetDefault();

			_pinIn1 = _gpio.OpenPin(5);
			_pinIn1.SetDriveMode(GpioPinDriveMode.Output);
			_pinIn1.Write(GpioPinValue.Low);

			_pinIn2 = _gpio.OpenPin(6);
			_pinIn2.SetDriveMode(GpioPinDriveMode.Output);
			_pinIn2.Write(GpioPinValue.Low);

			_pwm = new SoftPwm();

			tbFrequency.Text = _pwm.ActualFrequency.ToString();

			tbFrequencyMin.Text = _pwm.MinFrequency.ToString();
		    tbFrequencyMax.Text = _pwm.MaxFrequency.ToString();

		    sldFrequency.Minimum = _pwm.MinFrequency;
		    sldFrequency.Maximum = _pwm.MaxFrequency;
        }

		private void tgbIn1_Click(object sender, RoutedEventArgs e)
		{
			_pinIn1.Write(tgbIn1.IsChecked.GetValueOrDefault() ? GpioPinValue.High : GpioPinValue.Low);
		}

		private void tgbIn2_Click(object sender, RoutedEventArgs e)
		{
			_pinIn2.Write(tgbIn2.IsChecked.GetValueOrDefault() ? GpioPinValue.High : GpioPinValue.Low);
		}

		private void sldFrequency_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
		{
			if (_pwm != null)
			{
				double freq = _pwm.SetDesiredFrequency(sldFrequency.Value);

				tbFrequency.Text = freq.ToString();
			}
		}

	    private void sldDuty_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
	    {
		    tbDutyValue.Text = sldDuty.Value.ToString();
		    if (_pwm != null)
		    {
			    _pwm.SetPulseParameters(13, sldDuty.Value / 100, false);
		    }
	    }

	    private void btnStart_Click(object sender, RoutedEventArgs e)
		{
			_pwm.AcquirePin(13);
			_pwm.EnablePin(13);

			_pwm.SetPulseParameters(13, sldDuty.Value / 100, false);
			double freq = _pwm.SetDesiredFrequency(sldFrequency.Value);
			tbFrequency.Text = freq.ToString();
		}

		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			Application.Current.Exit();
		}

		private void btnStop_Click(object sender, RoutedEventArgs e)
		{
			_pwm.DisablePin(13);
			_pwm.ReleasePin(13);
		}
	}
}
